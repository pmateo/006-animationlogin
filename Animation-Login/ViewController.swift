//
//  ViewController.swift
//  Animation-Login
//
//  Created by Pablo Mateo Fernández on 14/12/2016.
//  Copyright © 2016 355 Berry Street S.L. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var backroundImage: UIImageView!
    @IBOutlet var titulo: UILabel!
    @IBOutlet var subtitulo: UIStackView!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var boton: UIButton!
    @IBOutlet var recordatorio: UILabel!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

